import '../styles/InfoCard.css'
import * as React from 'react';
import {chartStore} from "../stores/chartStore";
import { observer } from "mobx-react"
import {toJS} from "mobx";

const SelectedData = observer(() => {
   if (!chartStore.selectedData) {
       return <div className='InfoCard__subtitle'>None</div>
   }

   const curData = toJS(chartStore.selectedData);

   return <>
       <div className='InfoCard__subtitle'>{curData.month}</div>
       <div className='InfoCard__subtitle'>{curData.val} tkr</div>
   </>;
});

export const InfoCard = observer(() => {
    return <div className='InfoCard'>
        <div className='InfoCard__title'>You selected</div>
        <SelectedData/>
    </div>
});