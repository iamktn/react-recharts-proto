import {
    CartesianGrid,
    Line,
    LineChart, LineProps,
    ReferenceLine, ResponsiveContainer,
    Tooltip,
    Text,
    XAxis,
    YAxis, LabelList, LabelProps,
} from "recharts";

import '../styles/RechartsChart.css';
import { chartStore, SeriesData } from "../stores/chartStore";
import { observer } from "mobx-react";
import {PureComponent} from "react";
import {MouseEvent} from "react";

const Empty = () => {
    return <></>
}

type LargeDotProps = {
    radius: number;
    fill: string;
}

const LargeDot = (props: LineProps & LargeDotProps) => {
    const {cx, cy, stroke, strokeWidth, radius, fill} = props;
    if (!cy) {
        return null;
    }
    return <svg>
        <circle cx={cx} cy={cy} r={radius} stroke={stroke} fill={fill} strokeWidth={strokeWidth}/>
    </svg>
}

const AxisLabel = (props: { text: string }) => {
    return <Text
        className='ChartContainer__y-axis-label'
        textAnchor='start'
        x={0}
        y={0}
        dx={30}
        dy={20}>{props.text}</Text>
}

const formatter = (value: number) => {
    return `${value} tkr`;
}

type LineTooltipProps = {
    backgroundColor: string;
    dotColor: string;
    dotRadius: number;
    valueFormatter: (val: number) => string;
    visibleIndex: number | null;
}

const SomeLabel = (props: LabelProps & LineTooltipProps ) => {
    const width = 80;
    const height = 30;
    const deltaY = -50;
    const deltaX = 0;

    const {x, y, backgroundColor, dotColor, dotRadius, valueFormatter, value, visibleIndex } = props;
    if (!x || !y || !value || visibleIndex === null) {
        return null;
    }

    // @ts-ignore
    const index = props.index;
    if (index !== visibleIndex + 1) {
        return null;
    }

    const xNum = Number(x);
    const yNum = Number(y);

    const centerX = xNum + deltaX;
    const centerY = yNum + deltaY;

    return <svg>
        <circle cx={xNum} cy={yNum} r={dotRadius} fill={dotColor} strokeWidth={0}/>
        <rect x={centerX - width/2} y={centerY - height/2} width={width} height={height} style={{fill: backgroundColor}}/>
        <text x={centerX} y={centerY} textAnchor='middle' alignmentBaseline='central' style={{fontWeight: 'bold'}}>{valueFormatter(Number(value))}</text>
    </svg>
}

const OFFSET_PERCENT = 0.10;

@observer
export default class RechartsChart extends PureComponent {
    getDomain = () => {
        const min = chartStore.minValue;
        const max = chartStore.maxValue;

        if (!min || !max || min! === max!) {
            return ['dataMin', 'dataMax'];
        }

        const delta = max! - min!;

        return [ min! - delta*OFFSET_PERCENT, max! + delta*OFFSET_PERCENT ]
    }

    convertDataForDisplay = (data: SeriesData[]): SeriesData[] => {
        return [{ month: '', val: null }, ...data];
    }

    handleClick = (args: MouseEvent<SVGCircleElement>) => {
        // @ts-ignore
        const payload: SeriesData = args.payload;
        chartStore.setSelectedData(payload.month);
    }

    render() {
        return <ResponsiveContainer className='ChartContainer'>
            <LineChart className='ChartContainer__Chart' data={this.convertDataForDisplay(chartStore.data)}
                       margin={{top: 50, left: 20, right: 50}}>
                <XAxis fontFamily='Arial' dataKey="month" axisLine={false} tickLine={false} tick={{fill: '#000000'}}/>
                <YAxis fontFamily='Arial' dataKey="val" axisLine={false} tickLine={false} domain={this.getDomain()} label={<AxisLabel text='tkr'/>} tick={{fill: '#000000'}}/>
                <Tooltip content={<Empty/>} cursor={false} isAnimationActive={false}/>
                <CartesianGrid vertical={false} stroke='#E7E7E7' strokeDasharray='3 3'/>
                <ReferenceLine x='Apr' stroke='#EE2737' strokeWidth={2}/>
                <Line activeDot={{r: 15, onClick: (_, payload) => this.handleClick(payload)}} type="linear" dataKey="val" stroke="#141B4D"
                      strokeWidth="3" yAxisId={0} isAnimationActive={false} legendType="none" dot={<LargeDot fill='#ffffff' radius={10}/>}>
                    <LabelList dataKey="val"
                               content={<SomeLabel
                                   valueFormatter={formatter}
                                   backgroundColor='#E4E4E4'
                                   dotColor='#141B4D'
                                   dotRadius={5}
                                   visibleIndex={chartStore.visibleIndex}/>
                               }/>
                </Line>
            </LineChart>
        </ResponsiveContainer>
    }
};
