import {action, computed, makeObservable, observable } from "mobx";

const WINDOW_WIDTH = 5;

export type SeriesData = {
    month: string,
    val: number | null,
}

export class ChartStore {
    selectedData: SeriesData | null = null;
    allData: SeriesData[] = [];
    selectedMonth: string | null = null;
    selectedIndex: number | null = null;
    currentIndex: number;

    constructor() {
        this.selectedMonth = null;
        this.selectedData = null;
        this.allData = [
            { month: 'Jan', val: 120 },
            { month: 'Feb', val: 180},
            { month: 'Mar', val: 250 },
            { month: 'Apr', val: 300 },
            { month: 'May', val: 310},
            { month: 'Jun', val: 310 },
            { month: 'Jul', val: 310 },
            { month: 'Aug', val: 400},
            { month: 'Sep', val: 450 },
            { month: 'Oct', val: 490 },
            { month: 'Nov', val: 600},
            { month: 'Dec', val: 720 },
        ];
        this.currentIndex = 0;
        makeObservable(this, {
            data: computed,
            canMoveBack: computed,
            canMoveForward: computed,
            visibleIndex: computed,
            moveBack: action,
            moveForward: action,
            setSelectedData: action,
            selectedIndex: observable,
            currentIndex: observable,
            selectedData: observable,
            minValue: computed,
            maxValue: computed,
        })
    }

    get data(): SeriesData[] {
        return this.allData.slice(this.currentIndex, this.currentIndex + WINDOW_WIDTH);
    }

    get canMoveBack(): boolean {
        return this.currentIndex > 0;
    }

    get canMoveForward(): boolean {
        return this.currentIndex + WINDOW_WIDTH < this.allData.length;
    }

    get visibleIndex(): number | null {
        if (this.selectedData) {
            const index = this.allData.findIndex(v => v.month === this.selectedData!.month);
            if (index >= 0) {
                return index - this.currentIndex;
            }
        }

        return null;
    }

    _updateSelectedData = () => {
        if (this.selectedData && !this.data.find(d => d.month === this.selectedData!.month)) {
            this.selectedData = null;
        }
    }

    moveBack = () => {
        if (this.canMoveBack) {
            this.currentIndex = this.currentIndex - 1;
            this._updateSelectedData();
        }
    }

    moveForward = () => {
        if (this.canMoveForward) {
            this.currentIndex = this.currentIndex + 1;
            this._updateSelectedData();
        }
    }

    setSelectedData(month: string | null) {
        if (month) {
            this.selectedData = this.data.find(d => d.month === month) || null;
        }
        else {
            this.selectedData = null;
        }
    }

    get minValue(): number | null {
        let res: number = Infinity;
        for (const el of this.allData) {
            if (el.val) {
                res = el.val! < res ? el.val! : res;
            }
        }

        return res === Infinity ? null : res;
    }

    get maxValue(): number | null {
        let res: number = -Infinity;
        for (const el of this.allData) {
            if (el.val) {
                res = el.val! > res ? el.val! : res;
            }
        }

        return res === -Infinity ? null : res;
    }
}

export const chartStore = new ChartStore();