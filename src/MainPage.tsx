import RechartsChart from "./components/RechartsChart";
import {Navigator} from './components/Navigator';
import './MainPage.css';
import React from "react";
import { InfoCard } from "./components/InfoCard";
import {chartStore} from "./stores/chartStore";
import { observer } from "mobx-react";

export const MainPage = observer(() => {
    return <div className='MainPage'>
        <Navigator canNavigateBack={chartStore.canMoveBack}
                   canNavigateForward={chartStore.canMoveForward}
                   onNavigateBack={chartStore.moveBack}
                   onNavigateForward={chartStore.moveForward}>
            <RechartsChart />
        </Navigator>

        <InfoCard/>
    </div>
});