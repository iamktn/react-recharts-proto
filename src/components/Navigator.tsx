import { observer } from "mobx-react";
import {FunctionComponent} from "react";
import '../styles/Navigator.css';

type Props = {
    onNavigateBack?: () => void;
    onNavigateForward?: () => void;
    canNavigateBack: boolean;
    canNavigateForward: boolean;
}

export const Navigator: FunctionComponent<Props> = observer((props) => {
    const forward = () => {
        if (props.onNavigateForward) {
            props.onNavigateForward()
        }
    }

    const back = () => {
        if (props.onNavigateBack) {
            props.onNavigateBack()
        }
    }

    const navBackClassName = `Navigator__button${ !props.onNavigateBack || !props.canNavigateBack ? ' Navigator__button_disabled' : '' }`
    const navForwardClassName = `Navigator__button${ !props.onNavigateForward || !props.canNavigateForward ? ' Navigator__button_disabled' : '' }`

    return <div className='Navigator'>
        <div className={navBackClassName} onClick={back}>&lt;</div>
        {props.children}
        <div className={navForwardClassName} onClick={forward}>&gt;</div>
    </div>
});